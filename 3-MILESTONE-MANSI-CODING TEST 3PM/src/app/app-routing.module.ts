import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingComponent } from './booking/booking/booking.component';
import { QueryComponent } from './query/query/query.component';
import { ViewComponent } from './view/view/view.component';
import { WelcomeComponent } from './welcome/welcome/welcome.component';

const routes: Routes = [
  {path:'book',
   component:BookingComponent},
   {path:'query',
   component:QueryComponent},
   {path:'view',
   component:ViewComponent},
   {path:'welcome',
   component:WelcomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
