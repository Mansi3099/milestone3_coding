import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  data:any;
  constructor(private apiservice:ServicesService) { }


  ngOnInit(): void {
    this.apiservice.get().subscribe(response=>{
      this.data=response;
      console.log(this.data);
  })
}

}
