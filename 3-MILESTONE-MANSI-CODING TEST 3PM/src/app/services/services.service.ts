import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  baseUrl:string="http://localhost:3000/health-member";

  constructor(private http:HttpClient) { }

  get(){
    return this.http.get(this.baseUrl);

  }
  post(appointment:any){
    return this.http.post(this.baseUrl,appointment);

  }
}
